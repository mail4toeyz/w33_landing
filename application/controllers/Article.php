<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Article extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('api_web');
	}

	public function index()
	{
		//-------header
		$uri = 'w3_Home';
		$return = $this->api_web->guzzle_get($uri);
		$response = json_decode(json_encode($return), true);

		if ($response['status'] == '200') {
			$data['val'] = $response['content']['data'];
		}

		//-------Tentang
		$uri2 = 'w33_about';
		$return2 = $this->api_web->guzzle_get($uri2);
		$response2 = json_decode(json_encode($return2), true);

		if ($response2['status'] == '200') {
			$data['about'] = $response2['content']['data'];
		}

		//-------Program
		$uri3 = 'w33_project';
		$return3 = $this->api_web->guzzle_get($uri3);
		$response3 = json_decode(json_encode($return3), true);

		if ($response3['status'] == '200') {
			$data['program'] = $response3['content']['data'];
		}

		//-------Artikel
		$uri4 = 'w33_article';
		$return4 = $this->api_web->guzzle_get($uri4);
		$response4 = json_decode(json_encode($return4), true);

		if ($response4['status'] == '200') {
			$data['artikel'] = $response4['content']['data'];
		}

		//-------contact
		$uri5 = 'w33_contact';
		$return5 = $this->api_web->guzzle_get($uri5);
		$response5 = json_decode(json_encode($return5), true);

		if ($response5['status'] == '200') {
			$data['contact'] = $response5['content']['data'];
		}


		$data['content'] = 'article';
		$this->load->view('_layout/index', $data);
	}

	public function detail($id)
	{

		//-------header
		$uri = 'w3_Home';
		$return = $this->api_web->guzzle_get($uri);
		$response = json_decode(json_encode($return), true);

		if ($response['status'] == '200') {
			$data['val'] = $response['content']['data'];
		}

		//-------contact
		$uri5 = 'w33_contact';
		$return5 = $this->api_web->guzzle_get($uri5);
		$response5 = json_decode(json_encode($return5), true);

		if ($response5['status'] == '200') {
			$data['contact'] = $response5['content']['data'];
		}

		//-------contact
		$uri6 = 'w33_category';
		$return6 = $this->api_web->guzzle_get($uri6);
		$response6 = json_decode(json_encode($return6), true);

		if ($response6['status'] == '200') {
			$data['lcategory'] = $response6['content']['data'];
		}

		//-------detail
		$uri7 = 'w33_article?filter[id]=' . $id . '&fields=*, category.category';
		$return7 = $this->api_web->guzzle_get($uri7);
		$response7 = json_decode(json_encode($return7), true);

		if ($response6['status'] == '200') {
			$data['pdetail'] = $response7['content']['data'][0];
		}

		//-------Artikel
		$uri8 = 'w33_article?sort=sort,-date_created&limit=4&filter={"id": { "_neq": "' . $id . '"}}';
		$return8 = $this->api_web->guzzle_get($uri8);
		$response8 = json_decode(json_encode($return8), true);

		if ($response8['status'] == '200') {
			$data['artikel'] = $response8['content']['data'];
		}

		$data['content'] = 'detail_article';
		$this->load->view('_layout/index', $data);
	}
}
