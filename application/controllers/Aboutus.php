<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aboutus extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('api_web');
	}

	public function index()
	{
		//-------contact
		$uri = 'w3_Home';
		$return = $this->api_web->guzzle_get($uri);
		$response = json_decode(json_encode($return), true);

		if ($response['status'] == '200') {
			$data['val'] = $response['content']['data'];
		}

		//-------contact
		$uri5 = 'w33_contact';
		$return5 = $this->api_web->guzzle_get($uri5);
		$response5 = json_decode(json_encode($return5), true);

		if ($response5['status'] == '200') {
			$data['contact'] = $response5['content']['data'];
		}

		//-------Tentang
		$uri2 = 'w33_about';
		$return2 = $this->api_web->guzzle_get($uri2);
		$response2 = json_decode(json_encode($return2), true);

		if ($response2['status'] == '200') {
			$data['about'] = $response2['content']['data'];
		}

		$data['content'] = 'aboutus';
		$this->load->view('_layout/index', $data);
	}
}
