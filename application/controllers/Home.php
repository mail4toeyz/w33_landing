<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('api_web');
	}

	public function index()
	{
		//-------header
		$uri = 'w3_Home';
		$return = $this->api_web->guzzle_get($uri);
		$response = json_decode(json_encode($return), true);

		if ($response['status'] == '200') {
			$data['val'] = $response['content']['data'];
		}

		//-------Tentang
		$uri2 = 'w33_about';
		$return2 = $this->api_web->guzzle_get($uri2);
		$response2 = json_decode(json_encode($return2), true);

		if ($response2['status'] == '200') {
			$data['about'] = $response2['content']['data'];
		}

		//-------Program
		$uri3 = 'w33_project';
		$return3 = $this->api_web->guzzle_get($uri3);
		$response3 = json_decode(json_encode($return3), true);

		if ($response3['status'] == '200') {
			$data['program'] = $response3['content']['data'];
		}

		//-------Artikel
		$uri4 = 'w33_article';
		$return4 = $this->api_web->guzzle_get($uri4);
		$response4 = json_decode(json_encode($return4), true);

		if ($response3['status'] == '200') {
			$data['artikel'] = $response4['content']['data'];
		}

		//-------contact
		$uri5 = 'w33_contact';
		$return5 = $this->api_web->guzzle_get($uri5);
		$response5 = json_decode(json_encode($return5), true);

		if ($response5['status'] == '200') {
			$data['contact'] = $response5['content']['data'];
		}

		//-------Promo
		$uri6 = 'w33_promo??filter[status]=published';
		$return6 = $this->api_web->guzzle_get($uri6);
		$response6 = json_decode(json_encode($return6), true);

		if ($response5['status'] == '200') {
			$data['promo'] = $response6['content']['data'];
		}


		$data['content'] = 'home';
		$this->load->view('_layout/index', $data);
	}

	public function detail($id)
	{
		//-------contact
		$uri = 'w3_Home';
		$return = $this->api_web->guzzle_get($uri);
		$response = json_decode(json_encode($return), true);

		if ($response['status'] == '200') {
			$data['val'] = $response['content']['data'];
		}

		//-------contact
		$uri5 = 'w33_contact';
		$return5 = $this->api_web->guzzle_get($uri5);
		$response5 = json_decode(json_encode($return5), true);

		if ($response5['status'] == '200') {
			$data['contact'] = $response5['content']['data'];
		}

		//-------promo
		$uri6 = 'w33_promo?filter[id]=' . $id;
		$return6 = $this->api_web->guzzle_get($uri6);
		$response6 = json_decode(json_encode($return6), true);

		if ($response6['status'] == '200') {
			$data['promo'] = $response6['content']['data'];
		}

		$data['content'] = 'promo';
		$this->load->view('_layout/index', $data);
	}
}
