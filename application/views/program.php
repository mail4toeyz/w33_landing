<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="portfolio sections-bg">
	<div class="container" data-aos="fade-up">

		<div class="section-header">
			<h2>Program</h2>
			<p>Quam sed id excepturi ccusantium dolorem ut quis dolores nisi llum nostrum enim velit qui ut et autem uia reprehenderit sunt deleniti</p>
		</div>

		<div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry" data-portfolio-sort="original-order" data-aos="fade-up" data-aos-delay="100">

			<!-- <div>
            <ul class="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-product">Product</li>
              <li data-filter=".filter-branding">Branding</li>
              <li data-filter=".filter-books">Books</li>
            </ul>
          </div> -->

			<div class="row gy-4 portfolio-container">
				<?php
				foreach ($program as $val) {
					echo '<div class="col-xl-4 col-md-6 portfolio-item filter-app">
					<div class="portfolio-wrap">
						<a href="https://ford.api.article33.or.id/assets/' . $val['image'] . '" data-gallery="portfolio-gallery-app" class="glightbox">
						<img src="https://ford.api.article33.or.id/assets/' . $val['image'] . '" class="img-fluid" alt=""></a>
						<div class="portfolio-info">
							<h4><a href="program/detail/' . $val['id'] . '" title="More Details">' . $val['title'] . '</a></h4>
							<p>' . $val['slug'] . '</p>
						</div>
					</div>
				</div>';
				}
				?>
			</div><!-- End Portfolio Container -->

		</div>

	</div>
</section><!-- End Portfolio Section -->