    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
    	<div class="container" data-aos="fade-up">

    		<div class="section-header">
    			<h2><?php echo $promo[0]['name']; ?></h2>
    		</div>

    		<div class="row gy-4">
    			<div class="col-lg-6">
    				<img src="https://ford.api.article33.or.id/assets/<?php echo $promo[0]['image']; ?>" class="img-fluid rounded-4 mb-4" alt="">
    			</div>
    			<div class="col-lg-6">
    				<p><?php echo $promo[0]['description']; ?></p>
    				<!-- <div class="position-relative mt-4">
    					<img src="https://ford.api.article33.or.id/assets/<?php echo $promo['image']; ?>" class="img-fluid rounded-4" alt="">
    					<a href="<?php echo $promo['video']; ?>" class="glightbox play-btn"></a>
    				</div> -->
    			</div>
    		</div>
    	</div>

    	</div>
    </section><!-- End About Us Section -->