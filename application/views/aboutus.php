    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
    	<div class="container" data-aos="fade-up">

    		<div class="section-header">
    			<h2><?php echo $about['title']; ?></h2>
    			<p><?php echo $about['slug']; ?></p>
    		</div>

    		<div class="row gy-4">
    			<div class="col-lg-6">
    				<div class="position-relative mt-6">
    					<img src="https://ford.api.article33.or.id/assets/<?php echo $about['image']; ?>" class="img-fluid rounded-4" alt="">
    					<a href="<?php echo $about['video']; ?>" class="glightbox play-btn"></a>
    				</div>
    			</div>
    			<div class="col-lg-6">
    				<p><?php echo $about['description']; ?></p>

    			</div>
    		</div>
    	</div>

    	</div>
    </section><!-- End About Us Section -->