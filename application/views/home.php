<!-- <pre>
		<?php
		// print_r($artikel);
		?>
</pre> -->
<!-- ======= Hero Section ======= -->
<section id="hero" class="hero">
	<div class="position-relative">

		<!-- slider -->
		<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" style="margin-top:-110px; background-color: #000;">
			<div class="carousel-indicators">
				<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
				<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
				<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
			</div>
			<div class="carousel-inner" style=" width:100%; height: 500px !important;">
				<div class="carousel-item active" style="background-size: cover; opacity: 0.6; background-image: url('https://waroeng33.com/wp-content/uploads/2022/02/Carousel-Komunitas.png')">

				</div>
				<div class="carousel-caption">
					<h5><?php echo $val['title_1']; ?></h5>
					<h3><?php echo $val['title_2']; ?></h3>
					<p><b><?php echo $val['title_3']; ?></b></p>
				</div>
				<div class="carousel-item" style="background-size: cover; opacity: 0.6; background-image: url('https://waroeng33.com/wp-content/uploads/2020/12/1-Madu-300x142.jpg')">
					<!-- <div class="carousel-caption">
							<h5>Second slide label</h5>
							<p>Some representative placeholder content for the second slide.</p>
						</div> -->
				</div>
				<div class="carousel-item" style="background-size: cover; opacity: 0.6; background-image: url('https://waroeng33.com/wp-content/uploads/2022/04/waroeng-berbagi-300x216.jpeg')">
					<!-- <div class="carousel-caption">
							<h5>Third slide label</h5>
							<p>Some representative placeholder content for the third slide.</p>
						</div> -->
				</div>
			</div>
			<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Previous</span>
			</button>
			<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Next</span>
			</button>
		</div>
		<!-- end slider -->

	</div>

	<div class="icon-boxes position-relative">
		<div class="container position-relative">
			<div class="row gy-4 mt-5">
				<?php
				foreach ($promo as $val) {
					echo '<div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
					<div class="icon-box">
						<img src="https://ford.api.article33.or.id/assets/' . $val['image'] . '" class="img-fluid">
						<h4 class="title"><a href="home/detail/' . $val['id'] . '" title="More Details" class="stretched-link">' . $val['name'] . '</a></h4>
					</div>
				</div>';
				}
				?>

				<!-- <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
					<div class="icon-box">
						<div class="icon"><i class="bi bi-easel"></i></div>
						<h4 class="title"><a href="" class="stretched-link">Promo 1</a></h4>
					</div>
				</div> -->
				<!--End Icon Box -->

				<!-- <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
					<div class="icon-box">
						<div class="icon"><i class="bi bi-people"></i></div>
						<h4 class="title"><a href="" class="stretched-link">Promo 2</a></h4>
					</div>
				</div> -->
				<!--End Icon Box -->

				<!-- <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="300">
					<div class="icon-box">
						<div class="icon"><i class="bi bi-person-video3"></i></div>
						<h4 class="title"><a href="" class="stretched-link">Promo 3</a></h4>
					</div>
				</div> -->
				<!--End Icon Box -->

				<!-- <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="500">
					<div class="icon-box">
						<div class="icon"><i class="bi bi-puzzle"></i></div>
						<h4 class="title"><a href="" class="stretched-link">Promo 4</a></h4>
					</div>
				</div> -->
				<!--End Icon Box -->

			</div>
		</div>
	</div>

	</div>
</section>
<!-- End Hero Section -->

<!-- ======= About Us Section ======= -->
<section id="about" class="about">
	<div class="container" data-aos="fade-up">

		<div class="section-header">
			<h2><?php echo $about['title']; ?></h2>
			<p><?php echo $about['slug']; ?></p>
		</div>

		<div class="row gy-4">
			<div class="col-lg-6">
				<?php echo $about['description']; ?>
			</div>
			<div class="col-lg-6">
				<div class="content ps-0 ps-lg-5">
					<div class="position-relative mt-4">
						<img src="https://ford.api.article33.or.id/assets/<?php echo $about['image']; ?>" class="img-fluid rounded-4" alt="">
						<a href="<?php echo $about['video']; ?>" class="glightbox play-btn"></a>
					</div>
				</div>
			</div>
		</div>

	</div>
</section><!-- End About Us Section -->

<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="portfolio sections-bg">
	<div class="container" data-aos="fade-up">

		<div class="section-header">
			<h2>Program</h2>
			<p>Quam sed id excepturi ccusantium dolorem ut quis dolores nisi llum nostrum enim velit qui ut et autem uia reprehenderit sunt deleniti</p>
		</div>

		<div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry" data-portfolio-sort="original-order" data-aos="fade-up" data-aos-delay="100">

			<!-- <div>
            <ul class="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-product">Product</li>
              <li data-filter=".filter-branding">Branding</li>
              <li data-filter=".filter-books">Books</li>
            </ul>
          </div> -->

			<div class="row gy-4 portfolio-container">
				<?php
				foreach ($program as $val) {
					echo '<div class="col-xl-4 col-md-6 portfolio-item filter-app">
					<div class="portfolio-wrap">
						<a href="https://ford.api.article33.or.id/assets/' . $val['image'] . '" data-gallery="portfolio-gallery-app" class="glightbox">
						<img src="https://ford.api.article33.or.id/assets/' . $val['image'] . '" class="img-fluid" alt=""></a>
						<div class="portfolio-info">
							<h4><a href="program/detail/' . $val['id'] . '" title="More Details">' . $val['title'] . '</a></h4>
							<p>' . $val['slug'] . '</p>
						</div>
					</div>
				</div>';
				}
				?>
			</div><!-- End Portfolio Container -->

		</div>

	</div>
</section><!-- End Portfolio Section -->

<!-- ======= Recent Blog Posts Section ======= -->
<section id="recent-posts" class="recent-posts sections-bg">
	<div class="container" data-aos="fade-up">

		<div class="section-header">
			<h2>Kegiatan</h2>
			<p>Dokumentasi Kegiatan dan Artikel yang ada pada Waroeng33</p>
		</div>

		<div class="row gy-4">
			<?php
			foreach ($artikel as $val) {
				echo '<div class="col-xl-3 col-md-6">
				<article>

					<div class="post-img">
						<img src="https://ford.api.article33.or.id/assets/' . $val['image'] . '" alt="" class="img-fluid">
					</div>
					<h2 class="title">
						<a href="blog-details.html">' . $val['title'] . '</a>
					</h2>
					<p class="post-category">' . $val['slug'] . '</p>
				</article>
			</div>';
			}
			?>

		</div><!-- End recent posts list -->

	</div>
</section><!-- End Recent Blog Posts Section -->

<!-- ======= Frequently Asked Questions Section ======= -->
<section id="faq" class="faq">
	<div class="container" data-aos="fade-up">

		<div class="row gy-4">

			<div class="col-lg-4">
				<div class="content px-xl-5">
					<h3><strong>Pertanyaan</strong> Yang Sring Diajukan</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
					</p>
				</div>
			</div>

			<div class="col-lg-8">

				<div class="accordion accordion-flush" id="faqlist" data-aos="fade-up" data-aos-delay="100">

					<div class="accordion-item">
						<h3 class="accordion-header">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-1">
								<span class="num">1.</span>
								Apa itu Waroeng33?
							</button>
						</h3>
						<div id="faq-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist">
							<div class="accordion-body">
								Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
							</div>
						</div>
					</div><!-- # Faq item-->

					<div class="accordion-item">
						<h3 class="accordion-header">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-2">
								<span class="num">2.</span>
								Berapa Lama Proyek berlangsung?
							</button>
						</h3>
						<div id="faq-content-2" class="accordion-collapse collapse" data-bs-parent="#faqlist">
							<div class="accordion-body">
								Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
							</div>
						</div>
					</div><!-- # Faq item-->

					<div class="accordion-item">
						<h3 class="accordion-header">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-3">
								<span class="num">3.</span>
								Manfaat Waroeng33?
							</button>
						</h3>
						<div id="faq-content-3" class="accordion-collapse collapse" data-bs-parent="#faqlist">
							<div class="accordion-body">
								Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
							</div>
						</div>
					</div><!-- # Faq item-->

					<div class="accordion-item">
						<h3 class="accordion-header">
							<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-4">
								<span class="num">4.</span>
								Sasaran dari Waroeng33?
							</button>
						</h3>
						<div id="faq-content-4" class="accordion-collapse collapse" data-bs-parent="#faqlist">
							<div class="accordion-body">
								Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
							</div>
						</div>
					</div><!-- # Faq item-->
				</div>

			</div>
		</div>

	</div>
</section><!-- End Frequently Asked Questions Section -->

<!-- ======= Clients Section ======= -->
<section id="clients" class="clients">
	<div class="container" data-aos="zoom-out">

		<div class="clients-slider swiper">
			<div class="swiper-wrapper align-items-center">
				<div class="swiper-slide"><img src="https://madrasahreform.kemenag.go.id/__statics/img/erkam_warna.png" class="img-fluid" alt=""></div>
				<div class="swiper-slide"><img src="https://madrasahreform.kemenag.go.id/__statics/img/content/logo/EMIS.png" class="img-fluid" alt=""></div>
				<div class="swiper-slide"><img src="https://madrasahreform.kemenag.go.id/__statics/img/content/logo/mc.png" class="img-fluid" alt=""></div>
				<div class="swiper-slide"><img src="https://madrasahreform.kemenag.go.id/__statics/img/content/logo/mrc.png" class="img-fluid" alt=""></div>
			</div>
		</div>

	</div>
</section><!-- End Clients Section -->