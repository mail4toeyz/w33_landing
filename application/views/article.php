<!-- ======= Recent Blog Posts Section ======= -->
<section id="recent-posts" class="recent-posts sections-bg">
	<div class="container" data-aos="fade-up">

		<div class="section-header">
			<h2>Kegiatan</h2>
			<p>Dokumentasi Kegiatan dan Artikel yang ada pada Waroeng33</p>
		</div>

		<div class="row gy-4">
			<?php
			foreach ($artikel as $val) {
				echo '<div class="col-xl-3 col-md-6">
				<article>

					<div class="post-img">
						<img src="https://ford.api.article33.or.id/assets/' . $val['image'] . '" alt="" class="img-fluid">
					</div>
					<h2 class="title">
						<a href="article/detail/' . $val['id'] . '">' . $val['title'] . '</a>
					</h2>
					<p class="post-category">' . $val['slug'] . '</p>
				</article>
			</div>';
			}
			?>

		</div><!-- End recent posts list -->

	</div>
</section><!-- End Recent Blog Posts Section -->