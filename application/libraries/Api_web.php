<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_web
{
	var $CI;
	public function __construct($params = array())
	{
		$this->CI = &get_instance();
		//$this->url = 'https://nyelam.rocktokom.co/api/';
		$this->url = 'https://ford.api.article33.or.id/items/';
		//$this->url = 'https://migrasi-api.e-nyelam.com/';
	}

	public function guzzle_auth($uri = null,  $params = array())
	{
		$uri = $this->url . $uri;
		$output = $this->guzzle_response('AUTH', $uri, $params);
		return $output;
	}

	public function guzzle_get($uri = null)
	{
		$uri = $this->url . $uri;
		$output = $this->guzzle_response('GET', $uri);
		return $output;
	}

	public function guzzle_post($uri = null, $params = array())
	{
		$uri = $this->url . $uri;
		$output = $this->guzzle_response('POST', $uri, $params);
		return $output;
	}

	public function guzzle_form_field($uri)
	{
		$uri = $this->url . $uri;
		$output = $this->guzzle_response('POST', $uri, array('form_params' => $params));
		return $output;
	}

	public function guzzle_response($method = '', $uri = null, $params = array())
	{
		$client = new \GuzzleHttp\Client();
		$output = array();
		try {
			switch ($method) {
				case 'POST':
					$response = $client->request('POST', $uri, array('form_params' => $params));
					break;
				case 'GET':
					$response = $client->request('GET', $uri);
					break;
				case 'AUTH':
					$response = $client->request('GET', $uri, ['auth' => ['ck_0146733bed8484fb54f53aa57b0f0fd0fbabf9d8', 'cs_d7cad71ccc3e1e54a26a95c2d36590c948856805']]);
					break;
			}
			$output = array(
				'status' => $response->getStatusCode(),
				'phrase' => $response->getReasonPhrase(),
				'header' => $response->getHeader('content-type'),
				'protocol' => $response->getProtocolVersion(),
				'content' => json_decode($response->getBody())
			);
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$output = $this->guzzle_error($e);
		} catch (GuzzleHttp\Exception\ServerException $e) {
			$output = $this->guzzle_error($e);
		} catch (GuzzleHttp\Exception\BadResponseException $e) {
			$output = $this->guzzle_error($e);
		} catch (\Exception $e) {
			$output = $this->guzzle_error($e);
		}

		return $output;
	}

	public function guzzle_error($e)
	{
		$response = $e->getResponse();
		$status = $response->getStatusCode();

		if ($status != '200') {
			$output = array(
				'status' => $status,
				'phrase' => $response->getReasonPhrase(),
				'header' => $response->getHeader('content-type'),
				'protocol' => $response->getProtocolVersion(),
				'content' => json_decode($response->getBody())
			);
		} else {
			$output = array();
		}

		return $output;
	}
}
